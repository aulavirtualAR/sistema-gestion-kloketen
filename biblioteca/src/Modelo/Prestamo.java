/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.util.*;
/**
 *
 * @author Tecnicalicos
 */
public class Prestamo {
    private int id,id_alumno,id_docente,id_recurso;
            private Date fec_prestamo,fec_devolucion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(int id_alumno) {
        this.id_alumno = id_alumno;
    }

    public int getId_docente() {
        return id_docente;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public int getId_recurso() {
        return id_recurso;
    }

    public void setId_recurso(int id_recurso) {
        this.id_recurso = id_recurso;
    }

    public Date getFec_prestamo() {
        return fec_prestamo;
    }

    public void setFec_prestamo(Date fec_prestamo) {
        this.fec_prestamo = fec_prestamo;
    }

    public Date getFec_devolucion() {
        return fec_devolucion;
    }

    public void setFec_devolucion(Date fec_devolucion) {
        this.fec_devolucion = fec_devolucion;
    }
            
}
