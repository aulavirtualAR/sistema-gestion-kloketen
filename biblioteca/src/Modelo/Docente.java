/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.util.*;
/**
 *
 * @author Tecnicalicos
 */
public class Docente {
    private int id,dni,domicilio_num,telefono,domicilio_piso;
            private String mail,nombre,ciudad,apellido,domicilio_calle,domicilio_depto,domicilio_barrio,domicilio_otro;
                    private Date fecha_nac;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public int getDomicilio_num() {
        return domicilio_num;
    }

    public void setDomicilio_num(int domicilio_num) {
        this.domicilio_num = domicilio_num;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getDomicilio_piso() {
        return domicilio_piso;
    }

    public void setDomicilio_piso(int domicilio_piso) {
        this.domicilio_piso = domicilio_piso;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDomicilio_calle() {
        return domicilio_calle;
    }

    public void setDomicilio_calle(String domicilio_calle) {
        this.domicilio_calle = domicilio_calle;
    }

    public String getDomicilio_depto() {
        return domicilio_depto;
    }

    public void setDomicilio_depto(String domicilio_depto) {
        this.domicilio_depto = domicilio_depto;
    }

    public String getDomicilio_barrio() {
        return domicilio_barrio;
    }

    public void setDomicilio_barrio(String domicilio_barrio) {
        this.domicilio_barrio = domicilio_barrio;
    }

    public String getDomicilio_otro() {
        return domicilio_otro;
    }

    public void setDomicilio_otro(String domicilio_otro) {
        this.domicilio_otro = domicilio_otro;
    }

    public Date getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(Date fecha_nac) {
        this.fecha_nac = fecha_nac;
    }
                    
}
