package Vista;
import java.util.*;
import Controlador.AbmAutor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author 
 */
public class VistaAutor {
    
    public static void main (String args[]){
        int opcion=0;
        String nomautor;
        Scanner lectura = new Scanner(System.in);
        AbmAutor autor = new AbmAutor();
        while (opcion!=3){
            menu();
            opcion = lectura.nextInt();
            switch (opcion){
                case 1:
                    System.out.print("Ingrese nombre de autor: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    nomautor = lectura.nextLine(); // Ingreso nombre de autor
                    System.out.println("El autor es: "+nomautor); 
                    
                    autor.addAutor(nomautor);
                    break;
                case 2:
                    System.out.println(" ");
                    System.out.println("######## Listado de autores #########");
                    ResultSet listado = autor.listar();
                    {
                        try {
                            while (listado.next()){
                            int id = listado.getInt("id");
                            String nomAutor = listado.getString("nombre");
                            System.out.println("ID: "+id+" Nombre: "+nomAutor);
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(VistaAutor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
            }
        }
    }
    
    public static void menu(){
        System.out.println("Menu ABM Autores");
        System.out.println("Seleccione una opción");
        System.out.println("1- Agregar autor");
        System.out.println("2- Listar autores");
        System.out.println("3- Salir");
    }

}
