/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.AbmAlumno;
import Modelo.Alumno;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tecnicalicos
 */
public class VistaAlumno {
     public static void main (String args[]) throws ParseException{
        int opcion=0;
        int dnialumno,id_cursoalumno,telalumno,anio_ingrealumno;
        String fecha_nacalumno;
        String nomalumno,apealumno,lugar_nacimientoalumno,nacionalidadalumno,localidadalumno,domicilioalumno;
        byte sexoalumno;
        Scanner lectura = new Scanner(System.in);
        AbmAlumno alumno = new AbmAlumno();
        Alumno a =new Alumno();
        while (opcion!=3){
            menu();
            opcion = lectura.nextInt();
            switch (opcion){
                case 1:
                    System.out.print("Ingrese nombre de alumno: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    nomalumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("El alumno es: "+nomalumno); 
                    
                     System.out.print("Ingrese el apellido: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    apealumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("Apellido: "+apealumno); 
                    
                     System.out.print("Ingrese dni: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    dnialumno = lectura.nextInt(); // Ingreso nombre de alumno
                    System.out.println("Dni: "+dnialumno); 
                    
                     System.out.print("Ingrese la id de curso: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    id_cursoalumno = lectura.nextInt(); // Ingreso nombre de alumno
                    System.out.println("La id de curso es: "+id_cursoalumno); 
                    
                     System.out.print("Ingrese lugar de nacimiento: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    lugar_nacimientoalumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("Lugar de nacimiento: "+lugar_nacimientoalumno); 
                    
                     System.out.print("Ingrese nacionalidad: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    nacionalidadalumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("Nacionalidad: "+nacionalidadalumno); 
                    
                     System.out.print("Ingrese localidad: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    localidadalumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("Localidad: "+localidadalumno); 
                    
                     System.out.print("Ingrese domicilio: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    domicilioalumno = lectura.nextLine(); // Ingreso nombre de alumno
                
                    System.out.println("El domicilio es: "+domicilioalumno); 
                    
                     System.out.print("Ingrese telefono: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    telalumno = lectura.nextInt(); // Ingreso nombre de alumno
                    System.out.println("Su telefono es: "+telalumno); 
                    
                     System.out.print("Ingrese año: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    anio_ingrealumno = lectura.nextInt(); // Ingreso nombre de alumno
                    System.out.println("El alumno ingreso en: "+anio_ingrealumno); 
                    
                     System.out.print("Ingrese fecha de nacimiento: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    fecha_nacalumno = lectura.nextLine(); // Ingreso nombre de alumno
                    System.out.println("Fecha de nacimiento: : "+fecha_nacalumno); 
                    
                     System.out.print("Ingrese sexo: ");
                    lectura = new Scanner(System.in); // Inicializo nuevamente el scanner
                    sexoalumno = lectura.nextByte(); // Ingreso nombre de alumno
                    System.out.println("El alumno es: "+sexoalumno); 
                    
                    Date fecha_nacimiento=a.generarFechaNacimiento(fecha_nacalumno);
                    alumno.addAlumno(nomalumno,apealumno,dnialumno,id_cursoalumno,lugar_nacimientoalumno,nacionalidadalumno,localidadalumno,domicilioalumno,telalumno,anio_ingrealumno,fecha_nacimiento,sexoalumno);
                    break;
                case 2:
                    System.out.println(" ");
                    System.out.println("######## Listado de alumnos #########");
                    ResultSet listado = alumno.listar();
                    {
                        try {
                            while (listado.next()){
                            int id = listado.getInt("id");
                            String nomAlumno = listado.getString("nombre");
                            System.out.println("ID: "+id+" Nombre: "+nomAlumno);
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(VistaAutor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
            }
        }
    }
    
    public static void menu(){
        System.out.println("Menu ABM Alumnos");
        System.out.println("Seleccione una opción");
        System.out.println("1- Agregar alumno");
        System.out.println("2- Listar alumnos");
        System.out.println("3- Salir");
    }
    
}
