/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Tecnicalicos
 */
public class prueba_fecha {
    
    /**
     * Formato de fecha.
     */
   
    
    public static void main(String[] args) {
        // Instancia de simple date format.
        String lectura;
        String FORMAT= "yyyy-MM-dd";
        System.out.print("Ingrese nombre de autor: ");
        Scanner lec = new Scanner(System.in);            
         FORMAT = lec.nextLine(); // Ingreso nombre de autor
         System.out.println("El autor es: "+FORMAT); 
        
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
        
        Date resultado = null;
        
        // Obtenemos una fecha a partir de un String.
        try {
            resultado = sdf.parse("yyyy-MM-dd");
        } catch (ParseException ex) {
            System.out.println("Error en el formato de fecha.");
        }
        
        // Imprime la fecha del dia de hoy formateada 2015-11-28
        System.out.println("Fecha formateada: " + resultado);
    }
 
}
