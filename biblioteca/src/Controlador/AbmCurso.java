
package Controlador;

import Modelo.Curso;

public class AbmCurso {
    
    public int addCurso(String anio, String turno, String division){
        Curso curso = new Curso();
        curso.setDivision(division);
        curso.setTurno(turno);
        curso.setAnio(anio);
        int resultado = curso.guardar();
        return resultado;
    }

    
}
