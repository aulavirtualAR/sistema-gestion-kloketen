package Modelo;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Leonardo Vegas
 * Colegio Provincial Kloketen
 * Espacio curricular: Base de datos
 * Introducción a Java conectando a Base de Datos Mysql  
 */
public class conexionMysql {
    
    String usuario,pass,bd,host;
    Statement statement;
    Connection connection;
    
    public conexionMysql(String usuario, String pass, String bd){
        // Carga de Driver Mysql Para poder utilizar los metodos necesarios para la conexion
        try { //Carga de Driver
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) { // Si falla la carga imprime un error por consola
            System.out.println("Error, no se ha podido cargar MySQL JDBC Driver");
        }
        // Definio las propiedades para la clase
        this.host = "jdbc:mysql://192.168.2.162:3306/";
        this.usuario = usuario;
        this.pass = pass;
        this.bd = bd;
        
        conectar();
    }
    
    public void conectar(){
        // Crear conexion a base de datos
        try {
            //Crea la conexion SQL y asigna el string de conexion a la propiedad connection para luego poder ser reautilizada en donde sea necesario
            this.connection = (Connection) DriverManager.getConnection(host+bd, usuario, pass);

        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    
    ResultSet consultaSQL(String sql){
        ResultSet rs = null;
        try{
            //Creo un objeto Statement que es el metodo encargado de ejecutar las consultas SQL
            this.statement = (Statement) connection.createStatement();
            // Asigno a la variable rs el resultado de la consulta SQL pasada por parametro
            rs = statement.executeQuery(sql);
        }catch (Exception ex){ // Si la ejecucion de la consulta falla, es capturada por ex
            System.out.println("Error al realizar la consulta SQL: "+ex.getMessage());
        }
        return rs;
    }
    // Ejecuta consultas SQL del tipo INSERT, UPDATE, DELETE
    public int actualizacionSQL(String SQL){
        int rs=0;
        try{
            //Creo un objeto Statement que es el metodo encargado de ejecutar las consultas SQL
            this.statement = (Statement) connection.createStatement();
            // Asigno a la variable rs el resultado de la consulta SQL pasada por parametro
            rs = statement.executeUpdate(SQL);
        }catch (Exception ex){ // Si la ejecucion de la consulta falla, es capturada por ex
            System.out.println("Error al realizar la consulta SQL: "+ex.getMessage());
        }
        return rs;
    }
    
    //Metodo para cerrar la conexión con el servidor MYSQL
    public void cerrarConexion(){
        try{
            this.statement.close();
            this.connection.close();
        }catch (Exception ex){
            System.out.println("Error al cerrar la conexión: "+ex);
        }
    }
}
