package Modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * @author 
 */

public class Autor {
    private int id;
    private String nombre;
    

    // Metodo constructor
    public Autor(){
        this.id = 0;
        this.nombre = "";
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public  void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public int guardar(){
        int consulta=0;
        String sql = "INSERT INTO `autores` (`id`, `nombre`) VALUES (NULL, '"+nombre+"')";
        conexionMysql conexion = new conexionMysql("leonardo", "Sentar0266", "biblioteca");
        try{
            consulta = conexion.actualizacionSQL(sql);
            
        }catch(Exception ex){
            System.out.println("Error en: "+ex);
            return consulta;
        }
        return consulta;
    }

    public ResultSet listaAutores(){
        ResultSet consulta=null;
        String sql = "SELECT * FROM autores";
        conexionMysql conexion = new conexionMysql("leonardo", "Sentar0266", "biblioteca");
        try{
            consulta = conexion.consultaSQL(sql);
            
        }catch(Exception ex){
            System.out.println("Error en: "+ex);
        }
        return consulta;
    }
}
